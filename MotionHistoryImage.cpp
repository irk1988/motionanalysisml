/*
 * Program to identify human activities in a video file.
 * Identify MHI in a video; Use feature extraction method on MHI; Use the output to train a ML algorithm
 *
 * MotionHistoryImage.cpp
 * Author: Immanuel Rajkumar Philip Karunakaran
 */
#include<iostream>
#include<fstream>
#include<ctime>
#include<highgui.hpp>
#include<video.hpp>
#include<imgproc.hpp>
#include<features2d.hpp>
#include<ml.hpp>
#include<nonfree.hpp>
#include<core.hpp>
#include<string>
#include<sstream>
#include<map>
#include <math.h>
#include <ctype.h>

#define VIDEO_DIR "/home/irk/SourceCode/VideoAnalytics/"
#define TRAINFILE "train.txt"
//#define TRAINFILE "train1.txt"
#define TESTFILE "test.txt"

using namespace cv;
using namespace std;

int last = 0;
const int N = 4;
vector<Mat> buf(N);

const double MHI_DURATION = 0.5;
const double MAX_TIME_DELTA = 0.5;
const double MIN_TIME_DELTA = 0.05;

Mat mhi;

class VideoFile {
public:
	int label;
	string filename;
	Mat MHI;
	vector<KeyPoint> MHIKeyPoints;
	Mat desc;

	VideoFile(int l, string fname) {
		label = l;
		filename = fname;
	}
};

class MotionAnalyzer {
private:
	vector<VideoFile> trainVideos;
	vector<VideoFile> testVideos;
	Ptr<FeatureDetector> featureDetector;
	Ptr<DescriptorExtractor> descExtractor;
	Ptr<DescriptorMatcher> descMatcher;
	Ptr<BOWImgDescriptorExtractor> bowImgDescExtractor;
	NormalBayesClassifier classifier;

	TermCriteria tc;
	Mat dictionary;
	int dictionarySize;

public:

	MotionAnalyzer() {
		/*
		 featureDetector = FeatureDetector::create("SURF");
		 descExtractor = DescriptorExtractor::create("SURF");
		 */
		featureDetector = FeatureDetector::create("SIFT");
		descExtractor = DescriptorExtractor::create("SIFT");
		descMatcher = DescriptorMatcher::create("FlannBased");
		//	descMatcher = DescriptorMatcher::create("BruteForce");

		bowImgDescExtractor = new BOWImgDescriptorExtractor(descExtractor,
				descMatcher);
		//tc = TermCriteria(); //default
		dictionarySize = 128;
		//dictionarySize = 64;

		tc = TermCriteria(CV_TERMCRIT_ITER, 10, 0.001);
	}

	int init_train(string filename) {
		std::stringstream ss;
		ss << VIDEO_DIR << filename;
		ss >> filename;
		ifstream fin(filename.c_str());
		string buf;
		while (fin >> buf) {
			ss.clear();
			ss << buf;
			int label;
			ss >> label;
			fin >> buf;
			VideoFile vtemp(label, buf);
			trainVideos.push_back(vtemp);
		}
		return 0;
	}

	int init_test(string filename) {
		std::stringstream ss;
		ss << VIDEO_DIR << filename;
		ss >> filename;
		ifstream fin(filename.c_str());
		string buf;
		while (fin >> buf) {
			ss.clear();
			ss << buf;
			int label;
			ss >> label;
			fin >> buf;
			VideoFile vtemp(label, buf);
			testVideos.push_back(vtemp);
		}
		return 0;
	}

	/*
	 * Routine to analyze each action from action set; Construct the MHI for each video by invoking the extractMHI routine
	 */
	int startAnalysis() {
		BOWKMeansTrainer bowTrainer(dictionarySize, tc, 1, 0.001);
		int n = trainVideos.size();
		Mat vocabularyDescriptors;
		std::stringstream ss;
		int count = 0;
		for (int i = 0; i < n; i++) {
			string filename;
			ss.clear();
			ss << VIDEO_DIR << trainVideos[i].filename;
			ss >> filename;
			std::cout << "\tReading file: " << filename << std::endl;
			Mat curMHI;
			curMHI = extractMHIFromFile(filename);
//			imshow("MHI Converted - " + filename, curMHI);
//			waitKey();
			trainVideos[i].MHI = curMHI;
			Mat curDesc = extractFeature(curMHI, filename, true);

			count++;
			vocabularyDescriptors.push_back(curDesc);
		}

		cout << "Count : " << count << endl;
		bowTrainer.add(vocabularyDescriptors);
		dictionary = bowTrainer.cluster();
		cout << "Dictionary Size:" << dictionary.rows << "x" << dictionary.cols
				<< endl;

		bowImgDescExtractor->setVocabulary(dictionary);
		vector<Mat> descriptors = bowTrainer.getDescriptors();
		int counts = 0;
		for (vector<Mat>::iterator iter = descriptors.begin();
				iter != descriptors.end(); iter++) {
			counts += iter->rows;
		}
		cout << "Clustering " << counts << " features" << endl;

		Mat trainingData(0, dictionarySize, CV_32FC1);
		Mat labels(0, 1, CV_32FC1);

		int count2 = 0;
		for (int i = 0; i < n; i++) {
			Mat curDesc = extractFeature(trainVideos[i].MHI,
					trainVideos[i].filename, false);
			if (curDesc.empty()) {
				cout << "\nEmpty" << endl;
			} else {
				count2++;
				trainingData.push_back(curDesc);
				labels.push_back((float) trainVideos[i].label);
			}
		}
		cout << "\nDone Processing - " << trainingData.rows << "x"
				<< labels.rows << " Count 2- " << count2 << endl;

		classifier.train(trainingData, labels);
		return 0;
	}

	/*
	 * Test the given data
	 */
	int testData() {
		cout << "Starting Testing Data" << endl;
		int n = testVideos.size();
		for (int i = 0; i < n; i++) {
			string filename;
			std::stringstream ss;
			ss << VIDEO_DIR << testVideos[i].filename;
			ss >> filename;
			std::cout << "\nReading file: " << filename << std::endl;
			Mat curMHI;
			curMHI = extractMHIFromFile(filename);

//			imshow("TEST-" + filename, curMHI);
//			waitKey();

			testVideos[i].MHI = curMHI;
			Mat curDesc;
			curDesc = extractFeature(curMHI, filename, false);
			testVideos[i].desc = curDesc;

			Mat evalData(0, 128, CV_32FC1);
			evalData.push_back(curDesc);
			cout << "\nSize of the descriptor in the test image : "
					<< evalData.rows << "x" << evalData.cols << endl;

			float result = 0.0f;
			Mat resultsMat;
			if (countNonZero(evalData) > 1)
				result = classifier.predict(evalData, &resultsMat);
			cout << "Result :" << testVideos[i].filename << " Computed: "
					<< result << "  Actual:" << testVideos[i].label << endl;
		}
		return 0;
	}

	/*
	 * Routine to extract the MHI information for a video; Returns the MHI;
	 */
	Mat extractMHIFromFile(string filename) {
		VideoCapture cap(filename);
		Mat history, curFrame;
		if (!cap.isOpened())
			std::cout << "\nUnable to open video file\n";
		while (1) {
			bool read = cap.read(curFrame);
			if (!read)
				break;
			update_mhi(curFrame, history, 30);
		}
		return history;
	}

	/*
	 * Given a MHI as input, this routine extracts the features using the SURF descriptor
	 */
	Mat extractFeature(Mat MHI, string filename, bool isTrainData) {
		vector<KeyPoint> keypoints;
		Mat desc(0, dictionarySize, CV_32FC1);
		//Mat desc;
		featureDetector->detect(MHI, keypoints);

		if (keypoints.size() == 0)
			std::cout << "\nUnable to Find Key Points";
		else if (isTrainData) {
			descExtractor->compute(MHI, keypoints, desc);
		} else if (!isTrainData) {
			bowImgDescExtractor->compute(MHI, keypoints, desc);
		}

		/* Display and test the keypoints detected */
		/*
		 Mat dest1;
		 drawKeypoints(MHI, keypoints, dest1, Scalar::all(-1),
		 DrawMatchesFlags::DEFAULT);

		 imshow("MHI Converted - " + filename, dest1);
		 waitKey();
		 */
		return desc;
	}

	void update_mhi(const Mat& img, Mat& dst, int diff_threshold) {
		double timestamp = (double) clock() / CLOCKS_PER_SEC; // get current time in seconds
		int idx1 = last, idx2;
		Mat silh, orient, mask, segmask;

		cvtColor(img, buf[last], CV_BGR2GRAY); // convert frame to grayscale
		//cv::normalize(gray_img, gray_img, 0, 255, cv::NORM_MINMAX);

		idx2 = (last + 1) % N; // index of (last - (N-1))th frame
		last = idx2;

		if (buf[idx1].size() != buf[idx2].size())
			silh = Mat::ones(img.size(), CV_8U) * 255;
		else
			absdiff(buf[idx1], buf[idx2], silh); // get difference between frames

		threshold(silh, silh, diff_threshold, 1, CV_THRESH_BINARY); // and threshold it
		if (mhi.empty())
			mhi = Mat::zeros(silh.size(), CV_32F);
		updateMotionHistory(silh, mhi, timestamp, MHI_DURATION); // update MHI
	}
};

/* Main Driver Program */
int main(void) {
	initModule_nonfree();
	MotionAnalyzer ma;
	ma.init_train(TRAINFILE);
	ma.startAnalysis();
	ma.init_test(TESTFILE);
	ma.testData();
	return 0;
}

